<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>say Hello</title>
</head>
<body>
    
    <?php 
    
    // Mesma estrutura de JavaScript
    // Return smp vai ser a ultima linha de código
    function sayHi($name, $age){

        return "Hello $name, you are $age <br>";
    }
    
    echo sayHi("Tom", 40);
    echo sayHi("Dave", 13);
    echo sayHi("Oscar", 80);

    ?>

</body>
</html>