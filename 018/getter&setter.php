<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Movies Rating</title>
</head>
<body>

    <?php 
    class Movie {
        public $title; // Deixa ser público, então posso usar fora da classe
        private $rating; // Não posso usar fora da classe

        function __construct($title, $rating) {
            $this -> title = $title;
            $this ->setRating($rating);
            
        }
        /* Como não posso usar fora da classe, preciso do get e set para poder puxar para fora da classe
        o Get é quem vai deixar q eu veja fora da classe. Em outras palavras, é o que permite que eu execute o comando 
        echo*/
        function getRating() {
            return $this -> rating;
        }

        /* Já o set é o que permite que eu limite o conteúdo da variável.
        Todo o objetivo aqui é proteger para que a variável n tenha conteúdo aleatório. Assim, 
        essa função vai delimitar os possíveis conteúdos das variáveis */

        // Uma opção é esse: usa for e if-> Mais pesado.
        function setRating($rating) {
            $array = array("G","PG","PG-13","R","NR");

            for ($i=0; $i <count($array) ; $i++) { 
                if($rating == $array[$i]) {
                    $this->rating = $rating;
                    break;
                } else {
                    $this->rating = "NR";
                }
            }

            // Outra opção. Mais leve, mas mais complexa se mto comprida
            
            // if ($rating == "G" || $rating == "PG" || $rating == "PG-13" || $rating == "R"  || $rating == "NR") {
            //     $this->rating = $rating;
            // } else{
            //     $this->rating = "NR";
            // }
        }
    }

    $avengers = new Movie("Avengers", "PG");

    echo $avengers -> getRating();

    ?>
    
    

</body>
</html>