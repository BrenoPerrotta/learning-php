<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="pt-br">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Questão 1</title>
</head>
<body>
    <?php 

   
    function iris($x0, $x1){
        if ($x0 <= 2.45){
            return "Setosa";
        } elseif($x0> 2.45 && $x1 <= 1.75){
            return "Versicolor";
        } else{
            return "Virginica";
        }
    }

    $flor = iris(3,2);
    echo $flor;

    ?>
    
</body>
</html>