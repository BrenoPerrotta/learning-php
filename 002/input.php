<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Criando meu input com php</title>
</head>
<body>
    <form action="input.php" method="get">
        Name: <input type="text" name="nome">
        <br>
        Age: <input type="number" name="idade">
        <input type="submit">
        <br>
    </form>

    Your name is <?php echo $_GET["nome"] ?>
    <br>
    Your age is <?php echo $_GET["idade"] ?>

</body>
</html>