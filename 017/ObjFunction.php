<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Honors</title>
</head>
<body>
    <?php 
    
    class student {
        var $name;
        var $major;
        var $gpa;

        function __construct($aname, $amajor, $agpa) {
            $this -> name = $aname;
            $this -> major = $amajor;
            $this -> gpa = $agpa;
            
        }
        // Precisa criar dentro da classe. Não é possível puxar as informações por fora.
        function hasHonors() {
            if($this->gpa >= 3.5){
                return "true";
            }
            return "false";
        }
    }
    

    $student1 = new student("Jim","Business","2.8");
    $student2 = new student("Pam","Art","3.6");

    echo $student2->hasHonors();
    
    ?>
    
</body>
</html>