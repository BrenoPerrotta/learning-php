<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Using include</title>
</head>
<body>
    
    <?php include "header.html" ?>
    <p>Hello World</p>
    <?php include "footer.html" ?>

</body>
</html>