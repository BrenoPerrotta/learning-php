<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Criating Book class</title>
</head>
<body>

    <?php
    // Criando uma classe de livros
    class Book {
        var $title;
        var $author;
        var $pages;
    }

    // Posso fazer dessa forma, mas fica grande e difícil de manutenção. A melhor forma é com construtor.
    $Book1 = new book;
    $Book1 -> title = "Harry Potter";
    $Book1 -> author = "JK Rowling";
    $Book1 -> pages = 400;

    echo $Book1 -> author;

    class Person {
        Var $name;
        Var $surname;
        Var $cpf;

        // Forma melhor de fazer, usando construtor fica mais fácil de criar classes e de fazer manutenção
        function __construct($aname, $asurname, $acpf) {
            $this -> name = $aname;
            $this -> surname = $asurname;
            $this -> cpf = $acpf;
            
        }

    }

    $person1 = new Person("Caterine", "Azevedo", "136.178.457-85");
    $person2 = new Person("Breno","Medeiros","147.258.369-78");

    echo $person1 -> name;
    echo $person2 -> name;

    ?>
    
</body>
</html>