<!DOCTYPE html>
<html lang="Pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fazendo uma calculadora melhor</title>
</head>
<body>

    <!-- Pequeno texto explicativo sobre a calculadora -->
    <h3>Olá, bem vindo! <br> Eu sou a sua calculadora! <br> Apenas sou um pouco limitada rs. Por favor, coloque apenas números com até 4 casas decimais (ex: 0.0001)</h3>


    <!-- Form do html para buscar as informações. É por ele que vamos receber os valores -->
    <form action="calculator2.php" method="post">
        Número 1: <input type="number" step="0.0001" name="num1" ><br>
        Número 2: <input type="number" step="0.0001" name="num2" ><br>
        Operador: <select name="op" >
            <option value="sum"> + </option>
            <option value="sub"> - </option>
            <option value="mult"> * </option>
            <option value="div"> / </option>
        </select><br>
        <input type="submit">
    </form>

    <?php 

        // Forma resumida de fazer if. isset($_POST['num1']) retorna true se estiver definido e false se não estiver.
        // Se $_POST['num1'] estiver definido, $num1 receberá o valor de $_POST['num1']. Caso contrário, $num1 receberá uma string vazia ''.
        $num1 = isset($_POST['num1']) ? $_POST['num1'] : '';
        $num2 = isset($_POST['num2']) ? $_POST['num2'] : '';
        $op = isset($_POST['op']) ? $_POST['op'] : '';

        // Vai verificar se um dos valores está vazio. Se sim, vai printar a mensagem abaixo e não vai deixar ir para a função calculator.
        function ifError($num1, $num2) {
            if(empty($num1) || empty($num2)){
                echo "Por favor, preencha todos os campos!";
                exit;
            }
        }

        // Função que faz a calculadora de fato
        function calculator($num1, $num2, $op){

            if($op == "sum") {
                return $num1 + $num2;
            } elseif ($op == "sub") {
                return $num1 - $num2;
            } elseif ($op == "mult") {
                return $num1 * $num2;
            } else {
                return $num1 / $num2;
            }
            
        }
        
        ifError($num1, $num2);
        echo "O Resultado da operação é: " .calculator($num1, $num2, $op);
    ?>
    
</body>
</html>