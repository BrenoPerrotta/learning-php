<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>select greater number</title>
</head>
<body>

    <?php 
    
    function getmax($num1, $num2, $num3) {
        if($num1 >= $num2 && $num1 >= $num3) {
            return $num1;
        } elseif ($num2 >= $num1 && $num2 >= $num3) {
            return $num2;
        } else {
            return $num3;
        }
        
    }

    // Outra forma de fazer, mas mais pesado
    // function getmax($num1, $num2, $num3) {

    //     if ($num1 >= $num2) {
    //         $nummax = $num1;
    //     } else {
    //         $nummax = $num2;
    //     }

    //     if ($nummax >= $num3) {
    //         return $nummax;
    //     } else {
    //         $nummax = $num3;
    //         return $nummax;
    //     }

    // }
    
    echo getmax(1001,1001,101);

    ?>
    
</body>
</html>