<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Madlibs</title>
</head>
<body>
    <form action="Madlibs.php" method="get">
    Color: <input type="text" name="color" > <br>
    PluralNoun: <input type="text" name="pluralNoun" > <br>
    Celebrity: <input type="text" name="celebrity" > <br>
    <input type="submit">
    </form>

    <?php 
    
    // Jeito mais fácil de puxar o get dentro do php
    // br dentro de echo 
    $color = $_GET["color"];
    $pluralNoun = $_GET["pluralNoun"];
    $celebrity = $_GET["celebrity"];

    echo "Roses are $color <br>";
    echo "$pluralNoun are blue <br>";
    echo "I love $celebrity"  
    
    ?>

</body>
</html>