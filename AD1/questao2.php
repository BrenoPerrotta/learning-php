<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Questão 2</title>
</head>
<body>
    <?php 

    function a($MinutoDosGols) {
        // Ordenando o array para poder comparar o array[$i] com o array [$i+1]
        sort($MinutoDosGols);
        // Para garantir que, caso não tenha minutos repetidos, o primeiro seja printado na tela com 1 gol marcado
        $MaximoDeGols = 1;
        $MinutoDoMaximo = $MinutoDosGols[0];
        $GolsNoMinuto = 1;
    
        for ($i = 0; $i < count($MinutoDosGols)-1; $i++) {

            if($MinutoDosGols[$i]==$MinutoDosGols[$i+1]){
                $GolsNoMinuto++;

            }else{
                $GolsNoMinuto = 1;
            }
            
            if($GolsNoMinuto>$MaximoDeGols){
                $MaximoDeGols = $GolsNoMinuto;
                $MinutoDoMaximo = $MinutoDosGols[$i];
            } 

            }

            echo "$MinutoDoMaximo \n";
            echo "$MaximoDeGols \n";
        }

    $array = array(3,4,5,3,4,5,4,5,4,5,5,6,5);

    a($array);
    
    ?>
</body>
</html>