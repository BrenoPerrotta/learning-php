<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Questão 1</title>
</head>
<body>

    <?php 

    // Questão 1 Letra a)
    function Hamming($bita, $bitb, $bitc, $bitd) {
        if($bita==1 && $bitb==1 && $bitd==1){
            $redundancia1 = 1;
        } elseif ($bita + $bitb + $bitd == 1) {
            $redundancia1 = 1;
        } else{
            $redundancia1 = 0;
        }
        if($bita==1 && $bitc==1 && $bitd==1){
            $redundancia2 = 1;
        } elseif ($bita + $bitc + $bitd == 1) {
            $redundancia2 = 1;
        } else{
            $redundancia2 = 0;
        }
        if($bitb==1 && $bitc==1 && $bitd==1){
            $redundancia3 = 1;
        } elseif ($bitb + $bitc + $bitd == 1) {
            $redundancia3 = 1;
        } else{
            $redundancia3 = 0;
        }

        echo "Os bits de redundência para ".$bita,$bitb,$bitc,$bitd." são: "
        . $redundancia1, $redundancia2, $redundancia3."<br>";
    }

    // Questão 1 Letra b)
    function stringHamming($bin) {
        validador($bin);
        $array = array();

        for ($i=0; $i <strlen($bin); $i++) { 
            $array[] = $bin[$i];
            if(count($array)==4){
                Hamming($array[0],$array[1],$array[2],$array[3]);
                $array = array();
            }
        }
    }

    // Questão 1 Letra c)
    function validador($string){
        $patern = ('/^(?:[01]{4})*$/');
            if(preg_match($patern,$string)==0) {
                echo "O valor passado não tem somente 0 e 1 e/ou não tem o tamanho 4 ou múltiplo de 4"; 
                exit;
            }
                
    }


    Hamming(0,1,1,1);
    stringHamming("011101110111");

    ?>


    
</body>
</html>