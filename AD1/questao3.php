<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Questão 3</title>
</head>
<body>

    <?php 

    // Questão 3 Letra a)

    // criando a classe Funcionário
    class Funcionario {
        private $nome;
        private $cpf;
        private $dataNascimento;
        private $salario;

        //métodos que permitam consultar e alterar cada uma dessas propriedades
        function __construct($nome, $cpf, $dataNascimento, $salario) {
            $this-> nome = $nome;
            $this-> cpf = $cpf;
            $this-> dataNascimento = $dataNascimento;
            $this-> salario = $salario;
        }

        function getnome() {
            return $this-> nome;
        }

        function getcpf() {
            return $this-> cpf;
        }

        function getdataNascimento() {
            return $this-> dataNascimento;
        }

        function getsalario() {
            return $this-> salario;
        }

        function setnome($nome){
            return $this-> nome = $nome;
        }

        function setcpf($cpf){
            return $this-> cpf = $cpf;
        }

        function setdataNascimento($dataNascimento){
            return $this-> dataNascimento = $dataNascimento;
        }

        function setslario($salario){
            return $this-> salario = $salario;
        }

    }

    // questão 3 Letra b)

    // Criando uma instância da classe Funcionario
    $Funcionario1 = new Funcionario("José Silva", "012.345.678-90","20/07/1967","R$ 2000,00");

    // Exibindo o resultado na tela
    echo $Funcionario1 ->getnome() .", ". $Funcionario1 ->getcpf() .", ". $Funcionario1 ->getdataNascimento() .", ". $Funcionario1 ->getsalario();

    ?>
    
</body>
</html>