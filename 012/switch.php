<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>switch time</title>
</head>
<body>
    <form action="switch.php" method="post">
        <p>What was your grade?</p>
        <input type="text" name="grade">
        <input type="submit">
    </form>
    
    <?php 

        $grade = isset($_POST['grade']) ? $_POST['grade'] : '';
    
        switch($grade) {
            case "A":
                echo "You did a great job!";
                break;
            case "B":
                echo "You did pretty good!";
                break;
            default:
                echo "Invalid grade";
        }
    ?>
</body>
</html>