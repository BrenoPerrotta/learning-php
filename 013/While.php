<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>creating loop</title>
</head>
<body>

    <?php 
        // Para fazer um loop q veja a condição antes de executar
        $index = 1;
        while($index<=5){
            echo "$index <br>";
            $index++;
        }

        echo "<br>";
        echo "<br>";

        // Para fazer um loop que leia a execução antes da condição
        do{
            echo "Outro while <br>";
            echo "$index <br>";
            $index++;
        } while($index<=5);

        echo "<br>";
        echo "<br>";
        
        echo "Agora, com Loop For <br>";

        $RandomNum = array(1,3,5,6,78,6);

        // 1 - Index e seu valor, 2- condição do For, 3- Incremento
        for ($i=0; $i <count($RandomNum) ; $i++) { 
            echo "$RandomNum[$i] <br>";
        }
    ?>
    
</body>
</html>